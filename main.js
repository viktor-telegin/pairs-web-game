(() => {
  const numbersArray = [{number: '1'},{number: '1'},{number: '2'},{number: '2'},{number: '3'},{number: '3'},{number: '4'},{number: '4'},{number: '5'},{number: '5'},{number: '6'},{number: '6'},{number: '7'},{number: '7'},{number: '8'},{number: '8'},{number: '9'},{number: '9'},{number: '10'},{number: '10'},{number: '11'},{number: '11'},{number: '12'},{number: '12'},{number: '13'},{number: '13'},{number: '14'},{number: '14'},{number: '15'},{number: '15'},{number: '16'},{number: '16'},{number: '17'},{number: '17'},{number: '18'},{number: '18'},];

  function shuffle(array) {
    for (let x = array.length - 1; x > 0; x--) {
      let j = Math.floor(Math.random() * (x + 1));
      [array[x], array[j]] = [array[j], array[x]];
    }
  }

  const container = document.createElement('div');
  container.id = 'container';
  const input = document.createElement('input')
  input.id = 'input';
  input.style.width = '250px';
  input.placeholder = 'Введите количество ячеек 4, 16 или 36';

  const inputForm = document.createElement('form')
  const inputButton = document.createElement('button')
  inputButton.style.width = '70px'
  inputButton.style.height = '30px'
  inputButton.textContent = 'выбрать'
  inputForm.append(input)
  inputForm.append(inputButton)
  document.body.append(inputForm)

  const restartButton = document.createElement('button');
  restartButton.id = 'restartButton';
  restartButton.style.width = '250px';
  restartButton.style.height = '50px';
  restartButton.style.borderRadius = '15px';
  restartButton.style.marginLeft = '100px';
  restartButton.textContent = 'Еще разок?';

  let itemsNumber;
  let pair = [];
  let itemPair = [];
  let clickCount = 0;
  inputForm.addEventListener('submit', function (e) {
    e.preventDefault();
    container.innerHTML = '';
    container.style.columnCount = 2;
    itemsNumber = input.value;
    if (itemsNumber == 4) {container.style.columnCount = 2;}
    if (itemsNumber == 16) {container.style.columnCount = 4;}
    if (itemsNumber == 36) {container.style.columnCount = 6;}

    let endGame = itemsNumber;

    numbers = numbersArray.slice(0, itemsNumber)
    shuffle(numbers);

    for (let i = 0; i < itemsNumber; i++){

      const item = document.createElement('div');
      item.classList.add('item');

      const itemText = document.createElement('span')
      itemText.classList.add('itemText')
      itemText.classList.add('hidden');
      itemText.textContent  = numbers[i].number;
      item.append(itemText)
      container.append(item)

      function showHide () {
        if (item.classList.contains('item-turned')) {return}
        clickCount++;
        item.classList.toggle('item-turned')
        itemText.classList.toggle('hidden');
        pair.push(itemText);
        itemPair.push(item);
        if (pair[0] == undefined || pair[1] == undefined) {
          return
        }
        if (pair[0].textContent !== pair[1].textContent) {
          alert('несовпад')
          setTimeout(() => pair[0].classList.toggle('hidden'), 1000);
          setTimeout(() => itemText.classList.toggle('hidden'), 1000);
          setTimeout(() => itemPair[0].classList.toggle('item-turned'), 1000);
          setTimeout(() => item.classList.toggle('item-turned'), 1000);
          if (clickCount % 2 == 0); {
            setTimeout(() => pair.splice(0, pair.length),1000);
            setTimeout(() => itemPair.splice(0, itemPair.length),1000);
          }
          return
        }
        if (pair[0].textContent == pair[1].textContent) {
          itemPair[0].removeEventListener('click', showHide);
          item.removeEventListener('click', showHide);
          endGame = endGame -2;
          alert('совпадение');
          if (clickCount % 2 == 0); {
            setTimeout(() => pair.splice(0, pair.length),1000);
            setTimeout(() => itemPair.splice(0, itemPair.length),1000);
            if (endGame == 0) {
              restartButton.addEventListener('click', () => location.reload());
              document.body.append(restartButton);
        }
          }
        }
      }


      item.addEventListener('click', showHide)
    }});
  document.body.append(container);


})();
